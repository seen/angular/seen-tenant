# Seen Tenant

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-tenant/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-tenant/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/05429d0267184c279ba967c6a917f0c2)](https://www.codacy.com/app/seen/seen-tenant?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-tenant&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-tenant/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-tenant/commits/develop)